% ==============================================================================
% TCC - Ramon Pereira de Matos
% Capítulo 2 - Referencial Teórico
% ==============================================================================
\chapter{Engenharia Web, FrameWeb e Frameworks}
\label{sec-referencial}
No princípio da \textit{World Wide Web}, os sites eram formados por simples conjunto de arquivos de hipertexto ligados que apresentavam informações de texto e gráficos limitados. Com passar do tempo as ferramentas de desenvolvimento foram tornando possível oferecer junto com as páginas de textos e gráficos uma capacidade computacional. Desta forma nascem as aplicações para Web conhecidas e tratadas neste trabalho como \textit{WebApps} (\textit{Web Applications}).

Durante a primeira década a partir do surgimento da Web o desenvolvimento das aplicações era feito de maneira \textit{ad-hoc}, sem uma metodologia ou processo para apoiar os seus criadores, assim o projeto se desenvolvia de uma maneira artística. Logo, torna-se essencial o uso dos conceitos apresentados na Engenharia de Software, pois apesar de podermos afirmar que elas são diferentes, as \textit{WebApps} são apenas uma dentre as diferentes categorias de software. O ambiente Web, no entanto, possui como características particulares: uso intensivo de redes, simultaneidade, disponibilidade, evolução continua, segurança, estética, etc.~\cite{pressmannSoftEng}.

Desde sua criação podemos perceber a evolução da Web transformando sofisticados ambientes computacionais e com a expansão da Internet podemos ter verdadeiros sistemas de informação dentro da Web espalhados por diversos lugares, os quais denominamos como WIS (\textit{Web-based Information Systems}).

Nas próximas seções vamos apresentar melhor o conceito da adaptação da Engenharia de Software aplicada na Web, os conceitos dos \textit{frameworks} que surgiram para agilizar e facilitar o desenvolvimento de \textit{WebApps} e o método FrameWeb proposto para auxiliar na construção dessas aplicações.

\section{Engenharia Web}

Engenharia Web (\textit{Web Engineering - WebE}), é a Engenharia de Software aplicada ao desenvolvimento Web~\cite{pressmannSoftEng}. Segundo~\citeonline{murugesan2001web} ``é o estabelecimento do uso de um tom cientifico, engenharia, princípios de gestão, abordagens disciplinadas e sistemáticas para o sucesso do desenvolvimento, implantação e manutenção de sistemas e aplicativos baseados na Web de alta qualidade.''

Em um curto espaço de tempo, a Internet e a \textit{World Wide Web} tonaram-se onipresentes nas nossas vidas, superando todos os avanços tecnológicos que presenciamos na nossa história e nos afetando diretamente. Aumentou enormemente o seu alcance e extensão e, hoje, está presente nas indústrias, viagens, bancos, educação e governo, que a utilizam para melhorar e aprimorar suas operações. O comércio eletrônico também cresceu e se expandiu além das fronteiras nacionais, hoje com o avanço das tecnologias sem fio e dispositivos habilitados para acesso à Internet permite que realizemos várias tarefas do nosso cotidiano através desses dispositivos, o que desencadeou uma nova onda de aplicativos Web móveis.

Neste momento, em que a maioria das pessoas depende de sistemas e aplicativos baseados na Web, estes precisam ter um bom desempenho e serem confiáveis e, para que isso ocorra, os desenvolvedores precisam de uma metodologia sólida, um processo disciplinado e repetitivo, melhores ferramentas de desenvolvimento e um conjunto de boas diretrizes. Para que essas expectativas sejam atendidas usa-se a Engenharia Web para minimizar os riscos, melhorar a capacidade de manutenção e a qualidade das \textit{WebApps}.

Ao contrário da percepção de alguns desenvolvedores de software e profissionais da Engenharia de Software, a Engenharia Web não é um clone da Engenharia de Software, entretanto, esta comunga de muitos princípios e acrescenta novas abordagens, metodologias, ferramentas, técnicas e diretrizes para atender os requisitos específicos dos sistemas baseados na Web. As \textit{WebApps} mudam rapidamente em seus requisitos, conteúdos, e funcionalidades durante o seu ciclo de vida, mais do que acontece normalmente no desenvolvimento dos sistemas tradicionais de software~\cite{ginige2001web}.

Como a Engenharia de Software possui métodos disciplinados, a Engenharia Web adapta esses métodos para a criação de \textit{WebApps}. A complexidade de se projetar e implementar um software com vários requisitos e muitos \textit{stakeholders}, como é o caso das aplicações Web, torna relevante a adoção de uma metodologia que possui uma abordagem incremental e interativa. Um processo desse modelo divide a concepção de software em interações. Em cada interação são realizadas as atividades de análise, projeto, implementação e testes.

O modelo de processo Engenharia Web inicia-se na especificação dos requisitos, onde captura-se tudo aquilo considerado importante para entrega da aplicação. A coleta de requisitos, captura todas as informações possíveis sobre as funções que o sistema deve executar (requisitos funcionais) e as restrições sob as quais este deve operar (requisitos não funcionais). Posteriormente, realiza-se a modelagem de análise.

Na criação do Modelo de Análise o intuito é prover uma descrição dos domínios de informação, funcional e comportamental necessários para um sistema baseado em computadores. Este modifica-se dinamicamente a medida que se aprende mais sobre o sistema a ser construído e o cliente entende melhor aquilo que necessita. Há cinco principais tipos de análise resultantes dessa modelagem, que são: \textbf{de conteúdo}, que identifica o espectro completo do conteúdo a ser fornecido pela \textit{WebApp}; \textbf{de interação}, descrevendo como os usuários interagem com a \textit{WebApp}; \textbf{funcional}, que define as operações aplicadas ao conteúdo as funções de processamento independente do conteúdo, mas necessária para usuário final;\textbf{de navegação}, definido a estratégia geral de navegação para a \textit{WebApp}; e \textbf{de configuração}, que descreve o ambiente e a infraestrutura nos quais a \textit{WebApp} reside.

Conduzidos pelos resultados da modelagem de análise, a fase de projeto da \textit{WebApp} foca seis tópicos principais: projeto de interface, projeto de estética, projeto de conteúdo, projeto de navegação, projeto arquitetural e projeto de componentes. Projeto é uma atividade da engenharia que conduz um produto de qualidade~\cite{pressmannSoftEng}.

Segundo~\citeonline{olsina2001specifying}, o conjunto de atributos de qualidade a serem considerados na aplicação para Web são: 

\begin{itemize}
	\item \textbf{Usabilidade:} caracteriza-se pela facilidade de compreensão geral do site, emprego do recurso de ajuda e \textit{feedback online}, planejamento da interface para que a mesma seja agradável esteticamente e que possua características especiais;
	
	\item \textbf{Funcionalidade:} a capacidade de busca e recuperação, as características de navegação a adaptação aos \textit{browsers} e as questões relacionadas ao domínio da aplicação;
	
	\item \textbf{Confiabilidade:} validação e recuperação dos dados de entrada dos usuários, recuperação de erros e processamento correto dos links;
	 
	\item \textbf{Eficiência:} o tratamento das questões relacionadas ao desempenho do tempo de resposta e as velocidades de geração de páginas e imagens;
	
	\item \textbf{Manutenibilidade:} existe uma rápida evolução tecnológica aliada à necessidade de atualização constante do conteúdo e das informações disponibilizadas na Web, logo o software Web deve ser fácil de corrigir, adaptar e estender.
\end{itemize}


Existem alguns métodos que podem ser aplicados no desenvolvimento de aplicações Web para facilitar a produção, como WAE~\cite{conallen2002building}, OOWS~\cite{pastor2003oows} e OOHDM~\cite{schwabe98anobject}. Dentro do contexto da Engenharia Web, foi proposto o método FrameWeb, um método que propõe a otimização dos processos da Engenharia Web para WebApp baseadas em \textit{frameworks}. Este método será apresentado na Seção~\ref{sec-FramWeb}.



\section{Frameworks}
Na década de 1980 houve uma popularização da linguagem Smalltalk, paralelamente às bibliotecas de classes, dando início à construção de \textit{Frameworks} de aplicação, que acrescentam às bibliotecas de classes os relacionamentos e interação entre as diversas classes que o compõem, promovendo a reutilização das linhas de código, assim como o projeto abstrato envolvendo o domínio de aplicação.

Os \textit{frameworks} permitem reutilizar toda arquitetura de um domínio específico, bem como componentes isolados. Com a intenção de promover a melhoria da produtividade, qualidade e manutenibilidade, foram desenvolvidos diversos \textit{frameworks} nas últimas décadas~\cite{maldonado2002padroes}. Na Engenharia Web, o desenvolvimento de WISs possui uma infraestrutura arquitetônica comum. Portanto, logo depois da criação dos primeiros sistemas foram desenvolvidos vários \textit{frameworks} que generalizavam essa infraestrutura, deste modo o \textit{framework} pode ser visto como um conjunto de componentes prontos que podem ser utilizados, mediante configuração, facilitando a produção de código na construção de sistemas Web~\cite{Souza2007}.

\citeonline{Souza2007} organiza os frameworks de desenvolvimento de WISs e seis categorias diferentes:
\begin{itemize}
	\item \textit{Frameworks} MVC (Controladores Frontais);
	\item \textit{Frameworks} Decoradores;
	\item \textit{Frameworks} de Mapeamento Objeto/Relacional;
	\item \textit{Frameworks} de Injeção de Dependência (Inversão de Controle);
	\item \textit{Frameworks} para Programação Orientada a Aspectos (AOP);
	\item \textit{Frameworks} para Autenticação e Autorização.
\end{itemize}

Nas próximas subseções apresentaremos uma introdução aos \textit{frameworks} MVC, por ter relação direta com a arquitetura proposta pelo FrameWeb e os \textit{frameworks} utilizados na implementação do SCAP.

\subsection{Frameworks MVC}
MVC é a abreviatura de \textit{Model-View-Controller}~\cite{gamma1994design}, ou Modelo\--Visão\--Controle. Desde o seu desenvolvimento esta arquitetura foi bem aceita nas diversas áreas da Engenharia de Software e é possivelmente, uma das mais utilizadas na atualidade para construção de aplicações Web. A Figura~\ref{fig-padrao-mvc} ilustra a arquitetura MVC.

\begin{figure}[h!]
	\centering
	\includegraphics[width=.60\textwidth]{figuras/fig-padrao-mvc} 
	\caption{Diagrama representando a arquitetura MVC~\cite{prado-2015}}
	\label{fig-padrao-mvc}
\end{figure}

O elemento visão é encarregado de realizar a representação gráfica dos dados contidos no modelo, realiza solicitação das atualizações do modelo e é responsável de receber os estímulos do usuário e enviá-los ao controlador para que as devidas solicitações sejam atendidas.
O modelo encapsula o estado da aplicação, através das chamadas de métodos ele pode ter o seu estado alterado pelo controlador ou consultado pela visão, no caso de alteração de estado o modelo notifica a visão sobre as alterações.
O controlador é responsável por receber os estímulos do usuário enviados pela visão, transformando essas ações em atualizações no modelo e seleciona a visão para a entrega das respostas.

Apesar de alta aplicabilidade desse padrão MVC em aplicações Web, sua forma teórica não se encaixa perfeitamente nessa plataforma, pois a camada de modelo se encontra no servidor Web e como a comunicação é sempre iniciada na direção cliente – servidor o modelo não pode notificar o elemento visão sobre alterações, visto que este se encontra no navegador. Porém, quando aplicado a Web o nome correto para esse padrão seria ``Controlador Frontal'' (\textit{Front Controller})~\cite{alur2003core}. A Figura~\ref{fig-contorlador-frontal-web} ilustra esse padrão arquitetônico.

\begin{figure}[h!]
\centering
\includegraphics[width=.85\textwidth]{figuras/fig-contorlador-frontal-web} 
\caption{Funcionamento do padrão arquitetônico ``Controlador Frontal''~\cite{Souza2007}.}
\label{fig-contorlador-frontal-web}
\end{figure}

O cliente realiza uma requisição HTTP por meio do navegador, que pode ser um pedido de leitura de uma página ou envio de dados para processamento. O servidor Web encarrega o controlador frontal de tratar a requisição e este passa a gerenciar todo o processo, realizando as devidas ações para que seja emitida uma resposta à requisição, encaminhando para a visão. Os \textit{frameworks} MVC oferecem a implementação dessa arquitetura facilitando com que esse seja aplicado diante de configurações~\cite{Souza2007}.

\subsection{Spring}
O Spring \textit{Framework}~\cite{johnson2004spring} fornece um modelo abrangente de programação e configuração para aplicativos corporativos modernos baseados em Java em qualquer tipo de plataforma de implantação. Um elemento chave do Spring é o suporte a infraestrutura no nível de aplicação para que as equipes possam se concentrar na construção da lógica de negócio do aplicativo, sem vínculos desnecessários com ambientes de implantação específicos.

Destacam-se entre as suas características, a injeção de dependências, programação orientada a aspectos (AOP - \textit{Aspect Oriented Programming}), Spring MVC, suporte para JDBC e JPA, etc. Desde o início já incluso no Spring Framework, o Spring Web MVC é uma estrutura original criada na API \textit{Servlet}. O Spring MVC, como é mais conhecido, foi planejado a partir da ideia de um padrão de controlador frontal suportando diversos fluxos de trabalho.

\subsection{Vaadin}
Vaadin~\cite{gronroos2017book} é uma plataforma de desenvolvimento que oferece todas as ferramentas necessárias para criar facilmente um aplicativo da Web. O Vaadin Framework é uma estrutura UI e tem seu código escrito em Java e executado na JVM do servidor, enquanto a sua UI é renderizada como HTML5 no navegador. Este \textit{framework} também torna automática a comunicação entre o servidor e navegador.

Provê uma forte abstração protegendo o desenvolvedor das dificuldades na criação de aplicativos Web, pois ele não precisa se preocupar com a comunicação cliente-servidor ao desenvolver a sua aplicação. Ele ainda possui uma serie de componentes de UI reutilizáveis e é extensível com HTML e JavaScript.

Dentre as diferenças do Vaadin e os demais \textit{frameworks} podemos destacar que este em suas aplicações padrão não possui uma navegação tradicional de uma aplicação Web, pois geralmente são executados em uma única página. Mas isso não se torna um problema devido ao Vaadin dispor de uma classe que pode normalmente implementar e gerenciar a navegação de uma aplicação. 

\section{FrameWeb}
\label{sec-FramWeb}

O FrameWeb~\cite{Souza2007} define-se como método de projeto para a construção de sistemas de informação Web (\textit {Web Information System} – WISs) baseado em \textit{frameworks}. O método admite que tipos diferentes de \textit{frameworks} serão utilizados no desenvolvimento da aplicação, definindo uma arquitetura básica para o WIS e propondo modelos que aproximam o uso dos \textit{frameworks} a implementação do sistema. Um dos motivos para a sua criação se deve ao fato que dentre as diversas propostas para Engenharia Web, como metodologias, métodos, linguagens, etc., nenhuma delas leva em conta o relacionamento dos aspectos característicos dos \textit{frameworks} comumente utilizados na construção de WISs.

% <Vítor> Não há, de fato, contribuições para outras fases do desenvolvimento de software. Portanto, é melhor só falar do projeto arquitetural mesmo.
%
%O método concentra-se na fase de projeto, logo, como o processo de desenvolvimento de software não se restringe apenas a esta única fase, o FrameWeb propõe o uso de um processo que contemple todas as fases conforme apresentado na figura \ref{fig-processo-software}.
%
%\begin{figure}[h!]
%\centering
%\includegraphics{figuras/fig-processo-software} 
%\caption{Processo de desenvolvimento de software simples sugerido por FrameWeb~\cite{Souza2007}.}
%\label{fig-processo-software}
%\end{figure}

A fase de Projeto concentra as propostas principais do método: (i) definição de uma arquitetura padrão que divide o sistema em camadas, de modo a se integrar bem com os \textit{frameworks} utilizados; (ii) proposta de um conjunto de modelos de projeto que trazem conceitos utilizados pelos \textit{frameworks} para esta fase do processo por meio da definição de uma linguagem especifica de domínio que faz com que os diagramas fiquem mais próximos da implementação~\cite{Souza2007,martinsmestrado15}.

Na fase de implementação a produção do código é facilitada pela utilização dos \textit{frameworks}, principalmente porque os modelos de projeto elaborados remetem a componentes relacionados a eles~\cite{Souza2007}.

\subsection{Arquitetura Web utilizada no FrameWeb}
A arquitetura lógica padrão WISs definida no FrameWeb é baseada no padrão arquitetônico \textit{Service Layer} (Camada de Serviço)~\cite{fowler2002patterns}. Como pode ser visto na Figura~\ref{fig-arquitetura-software}, esse sistema arquitetônico é dividido em três camadas: lógica de apresentação, lógica de negócio e lógica de acesso a dados.

\begin{figure}[h!]
	\centering
	\includegraphics{figuras/fig-arquitetura-software} 
	\caption{Arquitetura padrão para WIS baseada no padrão arquitetônico \textit{Service Layer}~\cite{fowler2002patterns}.}
	\label{fig-arquitetura-software}
\end{figure}

A primeira camada é formada pelos pacotes de Visão e Controle. O pacote Visão tem como principal objetivo a interação do cliente com o sistema e nela encontram-se as páginas Web, folhas de estilo, imagens e \textit{scripts} que executam do lado cliente. O pacote Controle é responsável pelo monitoramento dos estímulos enviados pelo cliente através dos elementos da Visão, nesse pacote também abrange as classes de controle e outros arquivos relacionados ao \textit{framework} Controlador Frontal. Estes dois pacotes possuem mutua dependência pois as respostas esperadas pelo cliente através dos estímulos provocados acionam a classe controle que apresenta informações contidas nas classes do Domínio.


Domínio e Aplicação são os pacotes que compõem a segunda camada, denominada Lógica de Negócio. O pacote Domínio contém classes que representam os conceitos do domínio do problema, identificados e modelados na fase de análise pelos diagramas de classes que são revisados na fase de projeto. A Aplicação tem como objetivo a implementação dos casos de uso definidos na fase de especificação de requisitos, para prover uma camada de serviço independente da camada de apresentação responsável pela a interface com o usuário.

Apesar da independência da interface com o usuário, o pacote Controle depende do pacote Aplicação para tonar possível que os estímulos provocados pelo usuário na Visão sejam transformados, pelas classes de Controle, em métodos no pacote Aplicação para desta maneira executar os casos de usos.

A lógica de acesso a dados, terceira e última camada, conta com um único pacote, Persistência, responsável pelo armazenamento de informações dos atributos de classes que precisam ser persistidas em mídia de longa duração, como banco de dados, arquivos etc. Com aplicação do FrameWeb espera-se a utilização de um \textit{framework} de mapeamento objeto/relacional por meio do padrão de projeto \textit{Data Access Object} (DAO)~\cite{alur2003core}, este adiciona uma camada de abstração que promove um independência da lógica de acesso a dados da tecnologia de persistência possibilitando uma fácil substituição do \textit{framework} ORM se necessário.


\subsection{Linguagem de modelagem de FrameWeb}

Na fase de projeto também são gerados componentes 	que serão implementados na próxima fase de produção do software. Para agilizar o trabalho dos programadores foi determinada uma linguagem de modelagem especifica que representasse diretamente os conceitos existentes nos \textit{frameworks}, devido à grande integração entre eles.

Em sua proposta original~\cite{Souza2007}, o FrameWeb utilizava a mesma abordagem utilizada por outras linguagens de modelagem para Web, como WAE~\cite{conallen2002building} e UWE~\cite{koch2000extending}, definindo extensões leves (\textit{lightweight extensions}) ao meta-modelo da UML para representar componentes típicos da plataforma Web e dos \textit{frameworks} utilizados, criando um perfil UML. Entretanto, a evolução do FrameWeb proposta por \citeonline{martinsmestrado15} define uma linguagem Específica de Domínio (\textit{Domain Specific Language} ou DSL) formal para o FrameWeb, por meio de metamodelos que estendem uma parte do metamodelo da UML. Tais metamodelos especificam a sintaxe abstrata da linguagem, descrevendo os conceitos e as regras de modelagem específicas do método e, consequentemente, permitem a criação de ferramentas e mecanismos para que esta linguagem possa ser utilizada de forma correta e direcionada. 

A seguir é apresentado um breve resumo dos modelos propostos no FrameWeb, todos eles baseados no diagrama de classes da UML:

\begin{itemize}
\item \textbf{Modelo de Entidades:} é a representação dos objetos de domínio do problema e o seu mapeamento para persistência em um banco de dados relacional;

\item \textbf{Modelo de Persistência:} é um diagrama que conduz a implementação das classes DAO existentes, mostrando todas as interfaces e suas implementações, juntamente com os métodos contidos em cada classe;

\item\textbf{Modelo de Navegação:} é um diagrama que representa os diferentes componentes que formam a camada de lógica de apresentação, como páginas Web, formulários HTML e as classe de controle do \textit{framework Front Controller}. Além de guiar a implementação das páginas e das classes de controle, dá clara noção da interação entre os pacotes de Visão e Controle;

\item \textbf{Modelo de Aplicação:} é um diagrama que representa as classes de serviços do sistema para conduzir a implementação dos casos de uso e a suas dependências. Revela ainda a configuração de dependência entre os pacotes de Controle, Aplicação e Persistência, bem como, determina quais as classes de ação dependem de quais classes de serviço.
\end{itemize}





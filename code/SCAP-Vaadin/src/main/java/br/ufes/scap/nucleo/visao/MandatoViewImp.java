package br.ufes.scap.nucleo.visao;

import java.text.ParseException;

import org.springframework.beans.factory.annotation.Autowired;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.DateField;
import com.vaadin.ui.Label;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;

import br.ufes.scap.secretaria.aplicacao.AplMandato;
import br.ufes.scap.secretaria.controle.MandatoControle;

@SpringView(name = "mandatoform")
public class MandatoViewImp extends CustomComponent implements ClickListener, View {
	Label titulo = new Label("Entre com as informações do Mandato:");
	TextField matricula = new TextField("Matricula:");
	DateField dataIniMandato = new DateField("Inicio do Mandato");
	DateField dataFimMandato = new DateField("Fim do Mandato");
	Button salvar = new Button("Salvar",this);
	
	@Autowired
	private MandatoControle mandatoControle;

	@Override
	public void enter(ViewChangeEvent event) {
		VerticalLayout layout = new VerticalLayout();
		setCompositionRoot(layout);

		salvar.addStyleName(ValoTheme.BUTTON_PRIMARY);
		layout.addComponents(new MenuView(),titulo, matricula, dataIniMandato, dataFimMandato,salvar);
	}

	@Override
	public void buttonClick(ClickEvent event) {
		try {
			mandatoControle.salva(matricula.getValue(), dataIniMandato.getValue().toString(), dataFimMandato.getValue().toString());
			getUI().getNavigator().navigateTo("main");
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}
}
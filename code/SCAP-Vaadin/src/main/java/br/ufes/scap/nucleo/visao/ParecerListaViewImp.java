package br.ufes.scap.nucleo.visao;

import org.springframework.beans.factory.annotation.Autowired;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.Grid;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;

import br.ufes.scap.nucleo.controle.ParecerControle;
import br.ufes.scap.nucleo.controle.ParecerLista;
import br.ufes.scap.nucleo.dominio.Parecer;

@SpringView(name="parecerlista")
public class ParecerListaViewImp extends CustomComponent implements View {

	Grid<ParecerLista> gridParece = new Grid<>();
	Label titulo = new Label("Lista de pareceres do afastamento:");
	
	@Autowired
	ParecerControle parecerControle;

	@Override
	public void enter(ViewChangeEvent event) {
		VerticalLayout layout = new VerticalLayout();
		setCompositionRoot(layout);
		
		gridParece.setItems(parecerControle.listar());

		gridParece.addColumn(ParecerLista::getNomeCriador).setCaption("Nome");
		gridParece.addColumn(ParecerLista::getData).setCaption("Data");
		gridParece.addColumn(ParecerLista::getJulgamento).setCaption("Julgamento");
		gridParece.addColumn(ParecerLista::getMotivo).setCaption("Motivo");
		gridParece.setSizeFull();

		layout.addComponents(new MenuView(),titulo,gridParece);

	}

}
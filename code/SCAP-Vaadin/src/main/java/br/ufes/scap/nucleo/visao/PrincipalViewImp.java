package br.ufes.scap.nucleo.visao;

import java.awt.Menu;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.Button;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.VerticalLayout;

import br.ufes.scap.nucleo.controle.UsuarioController;

@SpringView(name = "main")
public class PrincipalViewImp extends CustomComponent implements View {

	@Autowired
	UsuarioController userlogado;

	@PostConstruct
	void init() {
		VerticalLayout layout = new VerticalLayout();
		setCompositionRoot(layout);

		Button btnNovoUsuario = new Button("Cadastrar um novo usuario",
				e -> getUI().getNavigator().navigateTo("pessoaform"));
		Button btnBuscaUsuario = new Button("Proucura um usuario",
				e -> getUI().getNavigator().navigateTo("pessoabusca"));
		Button btnBuscaAfastamento = new Button("Proucura um afastamento",
				e -> getUI().getNavigator().navigateTo("afastamentobusca"));
		Button btnNovoAfastamento = new Button("Cadastrar um afastamneto",
				e -> getUI().getNavigator().navigateTo("afastamentoform"));

		if (userlogado.tipoPofessor()) {
			layout.addComponents(new MenuView(), btnBuscaUsuario, btnBuscaAfastamento,
					btnNovoAfastamento);
		} else {
			layout.addComponents(new MenuView(), btnNovoUsuario, btnBuscaUsuario, btnBuscaAfastamento);
		}
	}

	public void enter(ViewChangeEvent event) {

	}
}
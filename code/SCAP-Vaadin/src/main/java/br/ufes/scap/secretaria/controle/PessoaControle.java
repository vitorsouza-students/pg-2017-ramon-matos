package br.ufes.scap.secretaria.controle;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.spring.annotation.UIScope;
import com.vaadin.ui.UI;

import br.ufes.scap.nucleo.dominio.Pessoa;
import br.ufes.scap.secretaria.aplicacao.AplPessoa;

@SpringComponent
@UIScope
public class PessoaControle {
	List<Pessoa> listaPessoa;
	
	@Autowired
	AplPessoa aplpessoa;
	
	public void salvar(Pessoa pessoa, UI ui){
		aplpessoa.salvar(pessoa);
		ui.getNavigator().navigateTo("main");
	}
	
	public void busca(String nome,String sobreNome){
		this.listaPessoa = aplpessoa.buscaNome(nome, sobreNome);
	}

	public List<Pessoa> getListaPessoa() {
		return listaPessoa;
	}

	public void setListaPessoa(List<Pessoa> listaPessoa) {
		this.listaPessoa = listaPessoa;
	}

}

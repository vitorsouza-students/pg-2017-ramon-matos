package br.ufes.scap.nucleo.visao;

import org.springframework.beans.factory.annotation.Autowired;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;

import br.ufes.scap.nucleo.controle.RelatorControle;


@SpringView(name="relatorform")
public class RelatorViewImp extends CustomComponent implements View , ClickListener {
	Label titulo = new Label("Entre com as informações do relator:");
	TextField matricula = new TextField("Matricula");
	Button salvar = new Button("Salvar",this);
	
	@Autowired
	RelatorControle relatorControle;

	@Override
	public void enter(ViewChangeEvent event) {
		VerticalLayout layout = new VerticalLayout();
		setCompositionRoot(layout);
		layout.setMargin(true);
		layout.setSpacing(true);

		salvar.addStyleName(ValoTheme.BUTTON_PRIMARY);

		layout.addComponents(new MenuView(),titulo,matricula,salvar);
	}

	@Override
	public void buttonClick(ClickEvent event) {
		if(relatorControle.salva(matricula.getValue().toString())){
			getUI().getNavigator().navigateTo("main");
		}else{
			new Notification("Não foi possível cadastrar,\n não há cadastro referente as esta matricula ou \n os professores possuem relação de parentesco", Notification.TYPE_ERROR_MESSAGE).show(getUI().getPage());
		}
		
	}

}
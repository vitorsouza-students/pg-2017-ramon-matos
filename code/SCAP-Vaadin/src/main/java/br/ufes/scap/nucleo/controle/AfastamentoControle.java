package br.ufes.scap.nucleo.controle;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.ModelMap;

import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.spring.annotation.UIScope;
import com.vaadin.ui.UI;

import br.ufes.scap.nucleo.aplicacao.AplAfastamento;
import br.ufes.scap.nucleo.aplicacao.Usuario;
//import br.ufes.scap.nucleo.aplicacao.Usuario;
import br.ufes.scap.nucleo.dominio.Afastamento;
import br.ufes.scap.nucleo.dominio.Documento;
import br.ufes.scap.nucleo.dominio.Onus;
import br.ufes.scap.nucleo.dominio.SituacaoSolic;
import br.ufes.scap.nucleo.dominio.TipoAfastamento;
import br.ufes.scap.secretaria.aplicacao.AplDocumento;
import br.ufes.scap.secretaria.aplicacao.AplMandato;

@SpringComponent
@UIScope
public class AfastamentoControle {
	private String idAfastamento;
	private List<DocumentoLista> listaDocumento;
	private String notificacao = "";
	
	@Autowired
	private AplAfastamento aplAfastamento;

	@Autowired
	private AplDocumento aplDocumento;

	@Autowired
	private AplMandato aplMandato;
	
	@Autowired
	private Usuario usuarioWeb;

	public void salva(String nome_evento, String nome_cidade, TipoAfastamento tipo, Onus onusAfastamento,
			String data_iniAfast, String data_fimAfast, String data_iniEvento, String data_fimEvento, UI ui) throws Exception {
		Afastamento novoAfastamento = new Afastamento();

		SimpleDateFormat formatada = new SimpleDateFormat("yyyy/MM/dd");

		Calendar cal = Calendar.getInstance();
		Calendar cal2 = Calendar.getInstance();
		Calendar cal3 = Calendar.getInstance();
		Calendar cal4 = Calendar.getInstance();
		Calendar cal5 = Calendar.getInstance();

		novoAfastamento.setNome_evento(nome_evento);
		novoAfastamento.setNome_cidade(nome_cidade);
		novoAfastamento.setData_criacao(cal);
		cal2.setTime(formatada.parse(data_iniAfast.replaceAll("-", "/")));
		novoAfastamento.setData_iniAfast(cal2);
		cal3.setTime(formatada.parse(data_fimAfast.replaceAll("-", "/")));
		novoAfastamento.setData_fimAfast(cal3);
		cal4.setTime(formatada.parse(data_iniEvento.replaceAll("-", "/")));
		novoAfastamento.setData_iniEvento(cal4);
		cal5.setTime(formatada.parse(data_fimEvento.replaceAll("-", "/")));
		novoAfastamento.setData_fimEvento(cal5);
		aplAfastamento.salvar(novoAfastamento,usuarioWeb.getLogado(), tipo, onusAfastamento);
		ui.getNavigator().navigateTo("main");
	}

	public List<AfastamentoLista> buscar() {
		List<Afastamento> listaDAO = aplAfastamento.listaAfastamentos();
		List<AfastamentoLista> tabela = new ArrayList<AfastamentoLista>();
		SimpleDateFormat formatada = new SimpleDateFormat("dd/MM/yyyy");
		for (Integer i = 0; i < listaDAO.size(); i++) {
			if (!listaDAO.get(i).getSituacaoSolicitacao().getStatusAfastamento().equals("CANCELADO")
					&& !listaDAO.get(i).getSituacaoSolicitacao().getStatusAfastamento().equals("ARQUIVADO")) {
				AfastamentoLista elemento = new AfastamentoLista();
				elemento.setId_afastamento(listaDAO.get(i).getId_afastamento().toString());
				elemento.setNome_pessoa(listaDAO.get(i).getSolicitante().getNome() + " "
						+ listaDAO.get(i).getSolicitante().getSobreNome());
				elemento.setNome_cidade(listaDAO.get(i).getNome_cidade());
				elemento.setNome_evento(listaDAO.get(i).getNome_evento());
				elemento.setSituacaoSolicitacao(listaDAO.get(i).getSituacaoSolicitacao().getStatusAfastamento());
				elemento.setTipoAfastamento(listaDAO.get(i).getTipoAfastamento().getTipoAfastamento());
				elemento.setData_iniAfast(formatada.format(listaDAO.get(i).getData_iniAfast().getTime()));
				elemento.setData_fimAfast(formatada.format(listaDAO.get(i).getData_fimAfast().getTime()));
				tabela.add(elemento);
			}
		}
		return tabela;
	}

	public List<String> mostrar(UI ui) {
		Afastamento afastamento = aplAfastamento.buscaId(idAfastamento);
		
		if(afastamento == null){
			setNotificacao("Afastamento inexistente");
			ui.getNavigator().navigateTo("afastamentobusca");
			return null;
		}else{
			setNotificacao("");
		}
	
		DateFormat f = DateFormat.getDateInstance(DateFormat.FULL);
		List<DocumentoLista> tabela = new ArrayList<DocumentoLista>();
		List<Documento> listaDAO = aplDocumento.buscaPorAfastamento(afastamento.getId_afastamento().toString());
		SimpleDateFormat formatada = new SimpleDateFormat("dd/MM/yyyy");

		for (Integer i = 0; i < listaDAO.size(); i++) {
			DocumentoLista elemento = new DocumentoLista();
			elemento.setId(listaDAO.get(i).getId_documento().toString());
			elemento.setTitulo(listaDAO.get(i).getTituloDocumento());
			elemento.setArquivo(listaDAO.get(i).getNomeArquivo());
			elemento.setJuntada(formatada.format(listaDAO.get(i).getData_juntada().getTime()));
			elemento.setContent(listaDAO.get(i).getContent());
			tabela.add(elemento);

		}

		this.listaDocumento = tabela;

		List<String> dadosAfastamento = new ArrayList<String>();
		dadosAfastamento
				.add(afastamento.getSolicitante().getNome() + " " + afastamento.getSolicitante().getSobreNome());
		dadosAfastamento.add(afastamento.getSolicitante().getMatricula());
		dadosAfastamento.add(afastamento.getSolicitante().getEmail());
		dadosAfastamento.add(afastamento.getSolicitante().getTelefone());
		dadosAfastamento.add(afastamento.getNome_cidade());
		dadosAfastamento.add(afastamento.getNome_evento());
		dadosAfastamento.add(afastamento.getTipoAfastamento().getTipoAfastamento());
		dadosAfastamento.add(afastamento.getOnus().getOnus());
		dadosAfastamento.add(afastamento.getSituacaoSolicitacao().getStatusAfastamento());
		dadosAfastamento.add(f.format(afastamento.getData_iniAfast().getTime()));
		dadosAfastamento.add(f.format(afastamento.getData_fimAfast().getTime()));
		dadosAfastamento.add(f.format(afastamento.getData_iniEvento().getTime()));
		dadosAfastamento.add(f.format(afastamento.getData_fimEvento().getTime()));
		dadosAfastamento.add(afastamento.getId_afastamento().toString());

		return dadosAfastamento;
	}
	
	public boolean mudarStatus(SituacaoSolic novoStatus){
		Afastamento afastamento = aplAfastamento.buscaId(idAfastamento);
		if(novoStatus.getStatusAfastamento().equals("CANCELADO")){
			if((usuarioWeb.getLogado().getMatricula().equals(afastamento.getSolicitante().getMatricula())) /*&& !(aplMandato.checaMandato(usuarioWeb.getLogado().getId().toString()))*/){
				aplAfastamento.mudarStatus(afastamento,novoStatus,usuarioWeb.getLogado());
				return true;
			}else{
				return false;
			}
		}
		aplAfastamento.mudarStatus(afastamento,novoStatus,usuarioWeb.getLogado());
		return true;
	}

	public String getIdAfastamento() {
		return idAfastamento;
	}

	public void setIdAfastamento(String idAfastamento) {
		this.idAfastamento = idAfastamento;
	}

	public List<DocumentoLista> getListaDocumento() {
		return listaDocumento;
	}

	public void setListaDocumento(List<DocumentoLista> listaDocumento) {
		this.listaDocumento = listaDocumento;
	}
	
	public String getNotificacao() {
		return notificacao;
	}

	public void setNotificacao(String notificacao) {
		this.notificacao = notificacao;
	}

}

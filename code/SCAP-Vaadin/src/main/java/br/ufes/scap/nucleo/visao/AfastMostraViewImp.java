package br.ufes.scap.nucleo.visao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.renderers.ButtonRenderer;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Panel;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;

import br.ufes.scap.nucleo.controle.AfastamentoControle;
import br.ufes.scap.nucleo.controle.DocumentoLista;
import br.ufes.scap.nucleo.controle.ParecerControle;
import br.ufes.scap.nucleo.controle.RelatorControle;
import br.ufes.scap.nucleo.controle.UsuarioController;
import br.ufes.scap.nucleo.dominio.Pessoa;

@SpringView(name = "afastmostrar")
public class AfastMostraViewImp extends CustomComponent implements View {
	Panel solicitante = new Panel("Solicitante");
	Panel evento = new Panel("Evento");
	Panel afastamento = new Panel("afastamento");

	Button mudaStatus = new Button("Mudar Status");
	Button definirParecer = new Button("Definir um Parecer");
	Button cadastrarRel = new Button("Cadastrar um Relator");
	Button cadastrarDoc = new Button("Cadastrar um Documento");
	Button verParecer = new Button("Ver pareceres");

	@Autowired
	AfastamentoControle afastControle;

	@Autowired
	ParecerControle parecerControle;

	@Autowired
	RelatorControle relatorControle;

	@Autowired
	UsuarioController userlogado;

	@Override
	public void enter(ViewChangeEvent event) {
		VerticalLayout layout = new VerticalLayout();
		setCompositionRoot(layout);

		layout.addComponent(new MenuView());

		addPainel(afastControle.mostrar(getUI()));
		Grid<DocumentoLista> gridDoc = new Grid<>();
		gridDoc.setItems(afastControle.getListaDocumento());

		HorizontalLayout paineis = new HorizontalLayout();
		layout.addComponent(paineis);
		paineis.addComponents(solicitante, evento, afastamento);
		paineis.setSizeFull();

		HorizontalLayout botoes = new HorizontalLayout();
		layout.addComponent(botoes);
		botoes.addComponents(definirParecer, mudaStatus, cadastrarRel, cadastrarDoc, verParecer);

		gridDoc.addColumn(DocumentoLista::getTitulo).setCaption("Titulo");
		gridDoc.addColumn(DocumentoLista::getJuntada).setCaption("Data Juntada");
		gridDoc.addColumn(e -> "Download", new ButtonRenderer<>(clickEvent -> {
		})).setCaption("Download arquivo");
		gridDoc.setSizeFull();

		layout.addComponent(gridDoc);

		mudaStatus.addClickListener(new ClickListener() {
			public void buttonClick(ClickEvent event) {
				getUI().getNavigator().navigateTo("statusform");
			}
		});

		definirParecer.addClickListener(new ClickListener() {
			public void buttonClick(ClickEvent event) {
				parecerControle.setIdAfastamento(afastControle.getIdAfastamento());
				getUI().getNavigator().navigateTo("parecerform");
			}
		});
		cadastrarRel.addClickListener(new ClickListener() {
			public void buttonClick(ClickEvent event) {
				relatorControle.setIdAfastamento(afastControle.getIdAfastamento());
				getUI().getNavigator().navigateTo("relatorform");
			}
		});
		cadastrarDoc.addClickListener(new ClickListener() {
			public void buttonClick(ClickEvent event) {
				getUI().getNavigator().navigateTo("documentoform");
			}
		});
		verParecer.addClickListener(new ClickListener() {
			public void buttonClick(ClickEvent event) {
				parecerControle.setIdAfastamento(afastControle.getIdAfastamento());
				getUI().getNavigator().navigateTo("parecerlista");
			}
		});
	}

	public void addPainel(List<String> dados) {
		VerticalLayout conteudoSolicitante = new VerticalLayout();
		conteudoSolicitante.addComponents(new Label(dados.get(0)), new Label("Matrícula: " + dados.get(1)),
				new Label("Email: " + dados.get(2)), new Label("Tel: " + dados.get(3)));
		solicitante.setContent(conteudoSolicitante);

		VerticalLayout conteudoEvento = new VerticalLayout();
		conteudoEvento.addComponents(new Label(dados.get(5)), new Label("Cidade: " + dados.get(4)),
				new Label(" Início: " + dados.get(11)), new Label("\n Fim: " + dados.get(12)));
		evento.setContent(conteudoEvento);

		VerticalLayout conteudoAfast = new VerticalLayout();
		conteudoAfast.addComponents(new Label("Status: " + dados.get(8)),
				new Label("Tipo: " + dados.get(6) + " Ônus: " + dados.get(7)), new Label("\n Início: " + dados.get(9)),
				new Label("Fim: " + dados.get(10)));
		afastamento.setContent(conteudoAfast);

	}

}

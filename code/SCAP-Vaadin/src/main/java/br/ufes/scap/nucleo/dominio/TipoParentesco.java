package br.ufes.scap.nucleo.dominio;

public enum TipoParentesco {
	MATRIMONIAL("MATRIMONIAL"),   
    SANGUINEO("SANGUINEO");
	
	private String tipoParentesco;  
	
	
	TipoParentesco(String tipo_Parentesco){  
	       this.tipoParentesco = tipo_Parentesco;  
	}  
	  
	public String getTipoAfastamento(){  
	       return tipoParentesco;  
	}
}
package br.ufes.scap.secretaria.controle;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.springframework.beans.factory.annotation.Autowired;

import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.spring.annotation.UIScope;

import br.ufes.scap.nucleo.dominio.Mandato;
import br.ufes.scap.secretaria.aplicacao.AplMandato;

@SpringComponent
@UIScope
public class MandatoControle {

	@Autowired
	private AplMandato aplMandato;

	public void salva(String matricula, String data_iniMandato, String data_fimMandato) throws ParseException {
		Mandato novoMandato = new Mandato();

		SimpleDateFormat formatada = new SimpleDateFormat("yyyy/MM/dd");
		Calendar cal = Calendar.getInstance();
		Calendar cal2 = Calendar.getInstance();

		cal.setTime(formatada.parse(data_iniMandato.replaceAll("-", "/")));
		novoMandato.setData_inicio(cal);
		cal2.setTime(formatada.parse(data_fimMandato.replaceAll("-", "/")));
		novoMandato.setData_fim(cal2);

		aplMandato.salvar(novoMandato, matricula);
	}

}

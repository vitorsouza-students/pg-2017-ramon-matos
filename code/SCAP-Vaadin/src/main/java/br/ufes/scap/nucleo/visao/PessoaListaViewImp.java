package br.ufes.scap.nucleo.visao;

import org.springframework.beans.factory.annotation.Autowired;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.Grid;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.renderers.ButtonRenderer;

import br.ufes.scap.nucleo.controle.ParecerControle;
import br.ufes.scap.nucleo.controle.UsuarioController;
import br.ufes.scap.nucleo.dominio.Pessoa;
import br.ufes.scap.secretaria.controle.ParentescoControle;
import br.ufes.scap.secretaria.controle.PessoaControle;

@SpringView(name = "pessoalist")
public class PessoaListaViewImp extends CustomComponent implements View {

	Grid<Pessoa> gridPessoa = new Grid<>();

	@Autowired
	UsuarioController userlogado;
	
	@Autowired
	PessoaControle pessoaControle;

	@Autowired
	ParentescoControle parentescoControle;

	@Override
	public void enter(ViewChangeEvent event) {
		VerticalLayout layout = new VerticalLayout();
		setCompositionRoot(layout);

		gridPessoa.setItems(pessoaControle.getListaPessoa());

		gridPessoa.addColumn(Pessoa::getNome).setCaption("Nome");
		gridPessoa.addColumn(Pessoa::getSobreNome).setCaption("Sobre Nome");
		gridPessoa.addColumn(Pessoa::getMatricula).setCaption("Matrícula");
		gridPessoa.addColumn(Pessoa::getEmail).setCaption("Email");
		gridPessoa.addColumn(Pessoa::getTelefone).setCaption("Telefone");
		if(!userlogado.tipoPofessor()){
		gridPessoa.addColumn(parentesco -> "Cadastrar", new ButtonRenderer<>(clickEvent -> {
			Pessoa aux = clickEvent.getItem();
			parentescoControle.setMatricula1(aux.getMatricula());
			getUI().getNavigator().navigateTo("parentescoform");
		})).setCaption("Cadastrar Parentesco");
		}

		gridPessoa.setSizeFull();

		layout.addComponents(new MenuView(), gridPessoa);
	}

}

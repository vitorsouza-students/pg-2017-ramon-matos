package br.ufes.scap.nucleo.controle;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import br.ufes.scap.nucleo.aplicacao.Usuario;
import br.ufes.scap.nucleo.dominio.Pessoa;
import br.ufes.scap.secretaria.aplicacao.AplPessoa;

@Controller
public class UsuarioController {
	
	@Autowired
	private Usuario usuarioWeb;
	
	@Autowired
	private AplPessoa aplPessoa;
	
	public boolean logar(String matricula, String password){
		Pessoa pessoa = new Pessoa();
		pessoa = aplPessoa.buscaMatricula(matricula);
		if(pessoa!=null){
			if(pessoa.getPassword().equals(password)){
				usuarioWeb.login(pessoa);
				return true;
			}else {
				return false;
			}
		}else{
			return false;
		}
	}
	
	public boolean tipoPofessor(){
		if(usuarioWeb.tipoPessoa().equals("1")){
			return true;
		}else{
			return false;
		}
	}
	
	
	public void logout() {
		return ;
	}
	
}

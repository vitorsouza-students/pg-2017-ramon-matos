package br.ufes.scap.nucleo.aplicacao;

import org.springframework.stereotype.Service;

import br.ufes.scap.nucleo.dominio.Pessoa;


@Service
public class Usuario {

	private Pessoa logado;
	
	public void login(Pessoa usuario) {
	    this.logado = usuario;
	}
	
	public boolean isLogado() {
	    return logado != null;
	}
	
	public String getMatricula(){
		return logado.getMatricula();
	}
	
	public Pessoa getLogado(){
		return logado;
	}
	
	public void logout(){
		this.logado = null;
	}
	
	public String tipoPessoa(){
		return this.logado.getTipoPessoa();
	}
	
}

package br.ufes.scap.nucleo.visao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.Grid;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.renderers.ButtonRenderer;
import com.vaadin.ui.themes.ValoTheme;

import br.ufes.scap.nucleo.controle.AfastamentoControle;
import br.ufes.scap.nucleo.controle.AfastamentoLista;

@SpringView(name = "afastamentobusca")
public class AfastBuscaViewImp extends CustomComponent implements View {
	Label titulo = new Label("Entre com o ID do Afastamento");
	TextField idAfast = new TextField("ID do Afastamneto");
	Grid<AfastamentoLista> gridAfast = new Grid<>();
	Button buscar = new Button("Buscar");

	@Autowired
	AfastamentoControle afastControle;

	@Override
	public void enter(ViewChangeEvent event) {
		VerticalLayout layout = new VerticalLayout();
		setCompositionRoot(layout);

		buscar.addStyleName(ValoTheme.BUTTON_PRIMARY);
		
		String aux2 = afastControle.getNotificacao();
		if (aux2.equals("Afastamento inexistente")) {
			new Notification(afastControle.getNotificacao(), Notification.TYPE_ERROR_MESSAGE).show(getUI().getPage());
		}

		List<AfastamentoLista> listaAfast = afastControle.buscar();
		gridAfast.setItems(listaAfast);

		gridAfast.addColumn(AfastamentoLista::getNome_pessoa).setCaption("Nome Solicitante");
		gridAfast.addColumn(AfastamentoLista::getId_afastamento).setCaption("ID do Afastamento");
		gridAfast.addColumn(AfastamentoLista::getNome_evento).setCaption("Nome do Evento");
		gridAfast.addColumn(AfastamentoLista::getSituacaoSolicitacao).setCaption("Status");
		gridAfast.addColumn(AfastamentoLista::getData_iniAfast).setCaption("Data de Iníco");
		gridAfast.addColumn(AfastamentoLista::getData_fimAfast).setCaption("Data de Fim");
		gridAfast.addColumn(afastamento -> "Ver", new ButtonRenderer<>(clickEvent -> {
			AfastamentoLista aux = clickEvent.getItem();
			afastControle.setIdAfastamento(aux.getId_afastamento());
			getUI().getNavigator().navigateTo("afastmostrar");
		}));

		gridAfast.setSizeFull();

		layout.addComponents(new MenuView(),titulo, idAfast, buscar, gridAfast);

		buscar.addClickListener(new ClickListener() {
			public void buttonClick(ClickEvent event) {
				afastControle.setIdAfastamento(idAfast.getValue());
				getUI().getNavigator().navigateTo("afastmostrar");
				
			}
		});

	}

}

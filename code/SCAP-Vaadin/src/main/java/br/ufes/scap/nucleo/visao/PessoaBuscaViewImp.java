package br.ufes.scap.nucleo.visao;

import org.springframework.beans.factory.annotation.Autowired;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.Label;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;

import br.ufes.scap.secretaria.aplicacao.AplPessoa;
import br.ufes.scap.secretaria.controle.PessoaControle;

@SpringView(name="pessoabusca")
public class PessoaBuscaViewImp extends CustomComponent implements ClickListener, View {
	Label titulo = new Label("Entre com o Nome  e Sobrenome da Pessoa:");
	TextField nome = new TextField("Nome");
	TextField sobreNome = new TextField("Sobre Nome");
	Button buscar = new Button("Buscar",this);
	
	@Autowired
	PessoaControle pessoaControle;

	@Override
	public void enter(ViewChangeEvent event) {
		VerticalLayout layout = new VerticalLayout();
		setCompositionRoot(layout);
		layout.setMargin(true);
		layout.setSpacing(true);

		buscar.addStyleName(ValoTheme.BUTTON_PRIMARY);

		layout.addComponents(new MenuView(),titulo, nome, sobreNome, buscar);
	}

	@Override
	public void buttonClick(ClickEvent event) {
		pessoaControle.busca(nome.getValue(), sobreNome.getValue());
		getUI().getNavigator().navigateTo("pessoalist");
	}
}

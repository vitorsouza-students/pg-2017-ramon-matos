package br.ufes.scap.nucleo.visao;

import org.springframework.beans.factory.annotation.Autowired;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.Label;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;

import br.ufes.scap.nucleo.controle.ParecerControle;
import br.ufes.scap.nucleo.dominio.TipoParecer;

@SpringView(name = "parecerform")
public class ParecerViewImp extends CustomComponent implements ClickListener, View {
	Label titulo = new Label("Entre com os dados do seu parecer:");
	TextArea motivo = new TextArea("Motivo");
	ComboBox<TipoParecer> tipoParecer = new ComboBox<TipoParecer>("Jugamento");
	Button salvar = new Button("Salvar",this);

	@Autowired
	ParecerControle parecerContorle;

	@Override
	public void enter(ViewChangeEvent event) {
		VerticalLayout layout = new VerticalLayout();
		setCompositionRoot(layout);

		tipoParecer.setItems(TipoParecer.values());
		tipoParecer.setTextInputAllowed(false);
		tipoParecer.setValue(TipoParecer.FAVORAVEL);
		tipoParecer.setEmptySelectionAllowed(false);

		salvar.addStyleName(ValoTheme.BUTTON_PRIMARY);

		layout.addComponents(new MenuView(),titulo, motivo, tipoParecer, salvar);
	}
	
	@Override
	public void buttonClick(ClickEvent event) {
		System.out.println(motivo.getValue().toString() + " "+ tipoParecer.getValue());
		parecerContorle.salvar(motivo.getValue().toString(), tipoParecer.getValue());
		
		getUI().getNavigator().navigateTo("main");
	}
}
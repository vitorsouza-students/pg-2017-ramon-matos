package br.ufes.scap.nucleo.visao;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;

public class MenuView extends CustomComponent implements View {

	public MenuView() {
		final VerticalLayout root = new VerticalLayout();
		root.setSizeFull();
		root.setMargin(false);
		root.setSpacing(false);
		setCompositionRoot(root);

		final CssLayout navigationBar = new CssLayout();
		navigationBar.setSizeFull();
		navigationBar.addStyleName(ValoTheme.LAYOUT_COMPONENT_GROUP);
		navigationBar.addComponent(createNavigationButton("Home", "main"));
		navigationBar.addComponent(createNavigationButton("Cadastrar Afastamento", "afastamentoform"));
		navigationBar.addComponent(createNavigationButton("Proucurar um usuario", "pessoabusca"));
		navigationBar.addComponent(createNavigationButton("Cadastrar um novo usuario", "pessoaform"));
		navigationBar.addComponent(createNavigationButton("Cadastrar um novo Mandato", "mandatoform"));
		navigationBar.setSizeFull();
		root.addComponent(navigationBar);
		root.setComponentAlignment(navigationBar, Alignment.TOP_CENTER);

	}

	@Override
	public void enter(ViewChangeEvent event) {

	}

	private Button createNavigationButton(String caption, final String viewName) {
		Button button = new Button(caption);
		button.addStyleName(ValoTheme.BUTTON_PRIMARY);
		button.addClickListener(event -> getUI().getNavigator().navigateTo(viewName));
		return button;
	}

}

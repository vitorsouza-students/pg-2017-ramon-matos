package br.ufes.scap.secretaria.controle;

import java.io.IOException;
import java.text.ParseException;
import java.util.Calendar;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.multipart.MultipartFile;

import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.spring.annotation.UIScope;

import br.ufes.scap.nucleo.dominio.Afastamento;
import br.ufes.scap.nucleo.dominio.Documento;
import br.ufes.scap.secretaria.aplicacao.AplDocumento;

@SpringComponent
@UIScope
public class DocumentoControle {

	@Autowired
	private AplDocumento aplDocumento;
	
	
	public void salva(String name,MultipartFile file) throws IOException, ParseException{
		Documento documento = new Documento();
		
		Afastamento afastamento = new Afastamento();
		/*afastamento = (Afastamento)session.getAttribute("afastamento");*/
		Calendar cal = Calendar.getInstance();
		
		documento.setData_juntada(cal);
		documento.setAfastamento(afastamento);
		documento.setTituloDocumento(name);
		documento.setNomeArquivo(file.getOriginalFilename());
		
		
		if (!file.isEmpty()) {
            byte[] bytes = file.getBytes();
            documento.setContent(bytes);
        }

		aplDocumento.salvar(documento);
		
	}
	
	public void download(){
		
	}
	/*public void downloadArquivo(@RequestParam("id_documento") String id,HttpServletResponse response) {
		try {
			// get your file as InputStream
		    Documento documento = aplDocumento.buscaId(id);
		    InputStream is = new ByteArrayInputStream(documento.getContent());
		    // copy it to response's OutputStream
		    org.apache.commons.io.IOUtils.copy(is, response.getOutputStream());
		    response.flushBuffer();
		} catch (IOException ex) {
			throw new RuntimeException("IOError writing file to output stream");   
		}	
	}*/
	/*
	public InputStreamDownload downloadArquivo(String id_documento){
		Documento documento = aplDocumento.buscaId(id_documento);
		if(documento!=null){
			result.include("teste"," não nulo");
			String contentType = "doc/pdf";
			InputStream aux = new ByteArrayInputStream(documento.getContent());
			InputStreamDownload stream = new InputStreamDownload(aux,contentType,documento.getNomeArquivo());
			return stream;
		}else{
			
		return null;
		}
	}*/
	
}

package br.ufes.scap.nucleo.visao;

import org.springframework.beans.factory.annotation.Autowired;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.DateField;
import com.vaadin.ui.Label;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;

import br.ufes.scap.nucleo.controle.AfastamentoControle;
import br.ufes.scap.nucleo.dominio.Onus;
import br.ufes.scap.nucleo.dominio.TipoAfastamento;

@SpringView(name = "afastamentoform")
public class AfastamentoViewImp extends CustomComponent implements ClickListener, View {
	Label titulo = new Label("Entre com as informações do Afastamento:");
	TextField nomeEvento = new TextField("Nome Evento:");
	TextField nomeCidade = new TextField("Nome Cidade:");
	DateField dataIniAfast = new DateField("Inicio do Afastamento");
	DateField dataFimAfast = new DateField("Fim do Afastamento");
	DateField dataIniEvento = new DateField("Inicio do Evento");
	DateField dataFimEvento = new DateField("Fim do Evento");
	ComboBox<Onus> onus = new ComboBox<Onus>("Ônus");
	ComboBox<TipoAfastamento> tipoAfast = new ComboBox<TipoAfastamento>("Tipo Afstamento");
	Button salvar = new Button("Salvar", this);

	@Autowired
	AfastamentoControle afastControle;

	@Override
	public void enter(ViewChangeEvent event) {
		VerticalLayout layout = new VerticalLayout();
		setCompositionRoot(layout);

		onus.setItems(Onus.values());
		onus.setTextInputAllowed(false);
		onus.setValue(Onus.INEXISTENTE);
		onus.setEmptySelectionAllowed(false);

		tipoAfast.setItems(TipoAfastamento.values());
		tipoAfast.setTextInputAllowed(false);
		tipoAfast.setValue(TipoAfastamento.NACIONAL);
		tipoAfast.setEmptySelectionAllowed(false);

		salvar.addStyleName(ValoTheme.BUTTON_PRIMARY);

		layout.addComponents(new MenuView(),titulo, nomeEvento, nomeCidade, dataIniAfast, dataFimAfast);
		layout.addComponents(dataIniEvento, dataFimEvento, tipoAfast, onus, salvar);

	}

	@Override
	public void buttonClick(ClickEvent event) {
		try {
			afastControle.salva(nomeEvento.getValue(), nomeCidade.getValue(), tipoAfast.getValue(), onus.getValue(),
					dataIniAfast.getValue().toString(), dataFimAfast.getValue().toString(),
					dataIniEvento.getValue().toString(), dataFimEvento.getValue().toString(),getUI());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}

package br.ufes.scap.nucleo.visao;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.Button;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.Label;
import com.vaadin.ui.TextField;
import com.vaadin.ui.Upload;
import com.vaadin.ui.Upload.Receiver;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;

@SpringView(name = "documentoform")
public class DocumentoViewImp extends CustomComponent implements View {
	Label titulo = new Label("Entre com os dados do documento");
	LineBreakCounter lineBreakCounter = new LineBreakCounter();

	TextField tituloDoc = new TextField("Título do Arquivo");
	Upload upLoad = new Upload("Arquivo", lineBreakCounter);
	Button salvar = new Button("Salvar");

	@Override
	public void enter(ViewChangeEvent event) {
		VerticalLayout layout = new VerticalLayout();
		setCompositionRoot(layout);

		salvar.addStyleName(ValoTheme.BUTTON_PRIMARY);

		layout.addComponents(new MenuView(), titulo, tituloDoc, upLoad, salvar);
	}

	private static class LineBreakCounter implements Receiver {
		File arquivo;

		@Override
		public OutputStream receiveUpload(final String filename, final String MIMEType) {
			return new OutputStream() {
				@Override
				public void write(final int b) {

				}
			};
		}

	}
}

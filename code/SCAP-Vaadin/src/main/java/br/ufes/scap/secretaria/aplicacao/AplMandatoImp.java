package br.ufes.scap.secretaria.aplicacao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.ufes.scap.nucleo.dominio.Mandato;
import br.ufes.scap.nucleo.dominio.Pessoa;
import br.ufes.scap.secretaria.persistencia.MandatoDAO;
import br.ufes.scap.secretaria.persistencia.PessoaDAO;

@Service
public class AplMandatoImp implements AplMandato{

	@Autowired
	private PessoaDAO pessoaDAO;
	
	@Autowired 
	private MandatoDAO mandatoDAO;
	
	@Transactional
	@Override
	public void salvar(Mandato novoMandato, String matricula) {
		Pessoa chefeDepatamento;
		chefeDepatamento = pessoaDAO.buscaMatricula(matricula);
		novoMandato.setPessoa(chefeDepatamento);
		mandatoDAO.salvar(novoMandato);
	}

	@Transactional
	@Override
	public boolean checaMandato(String id_pessoa) {
		return mandatoDAO.checaMandato(id_pessoa);
	}

}

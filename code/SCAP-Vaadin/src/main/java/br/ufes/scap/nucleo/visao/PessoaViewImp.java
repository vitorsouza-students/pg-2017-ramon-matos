package br.ufes.scap.nucleo.visao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.vaadin.data.Binder;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.Label;
import com.vaadin.ui.PasswordField;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;

import br.ufes.scap.nucleo.dominio.Pessoa;
import br.ufes.scap.secretaria.controle.PessoaControle;

@SpringView(name = "pessoaform")
public class PessoaViewImp extends CustomComponent implements View, ClickListener {

	Label titulo = new Label("Insira os dados da Pessoa");
	TextField nome = new TextField("Nome:");
	TextField sobreNome = new TextField("Sobre Nome:");
	TextField email = new TextField("Email:");
	TextField matricula = new TextField("Matrícula:");
	TextField telefone = new TextField("Telefone:");
	PasswordField password = new PasswordField("Password:");
	Button salvarPessoa = new Button("Salvar",this);
	ComboBox<String> tipoPessoa = new ComboBox<String>();

	Pessoa pessoa = new Pessoa();
	Binder<Pessoa> binder = new Binder<>(Pessoa.class);

	@Autowired
	PessoaControle pessoaControle;

	public void enter(ViewChangeEvent event) {
		VerticalLayout layout = new VerticalLayout();
		setCompositionRoot(layout);

		List<String> listTipoPessoa = new ArrayList<>();
		listTipoPessoa.add("Professor");
		listTipoPessoa.add("Secretário");

		binder.bindInstanceFields(this);
		binder.readBean(pessoa);

		tipoPessoa.setItems(listTipoPessoa);

		salvarPessoa.addStyleName(ValoTheme.BUTTON_PRIMARY);
		tipoPessoa.setTextInputAllowed(false);
		tipoPessoa.setValue(listTipoPessoa.get(0));
		tipoPessoa.setEmptySelectionAllowed(false);

	

		layout.addComponents(new MenuView(),titulo, nome, sobreNome, email, matricula, telefone, password, tipoPessoa, salvarPessoa);

	}
	
	@Override
	public void buttonClick(ClickEvent event) {
		if (binder.writeBeanIfValid(pessoa)) {
			if (pessoa.getTipoPessoa().equals("Professor")) {
				pessoa.setTipoPessoa("1");
			} else {
				pessoa.setTipoPessoa("2");
			}

			pessoaControle.salvar(pessoa,getUI());

			System.out.println(pessoa.getNome() + " " + pessoa.getSobreNome() + " " + pessoa.getEmail() + " "
					+ pessoa.getMatricula() + " " + pessoa.getTelefone() + " " + pessoa.getPassword() + " "
					+ pessoa.getTipoPessoa());
		}		
	}
}

package br.ufes.scap.nucleo.controle;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;

import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.spring.annotation.UIScope;
import com.vaadin.ui.UI;

import br.ufes.scap.nucleo.aplicacao.AplAfastamento;
import br.ufes.scap.nucleo.aplicacao.AplParecer;
import br.ufes.scap.nucleo.aplicacao.Usuario;
import br.ufes.scap.nucleo.dominio.Afastamento;
import br.ufes.scap.nucleo.dominio.Parecer;
import br.ufes.scap.nucleo.dominio.TipoParecer;

@SpringComponent
@UIScope
public class ParecerControle {
	private String idAfastamento;
	
	@Autowired
	private Usuario usuarioWeb;
	
	@Autowired
	private AplParecer	aplParecer;
	
	@Autowired
	private AplAfastamento aplAfastamento;
	
	public List<ParecerLista> listar(){
		
		List<Parecer> listaDAO = aplParecer.buscaPorAfastamento(idAfastamento);
		List<ParecerLista> tabela = new ArrayList<ParecerLista>();
		SimpleDateFormat formatada = new SimpleDateFormat("dd/MM/yyyy");
		for(Integer i=0;i<listaDAO.size();i++){
			ParecerLista elemento = new ParecerLista();
			elemento.setNomeCriador(listaDAO.get(i).getRelator().getNome()+" "+listaDAO.get(i).getRelator().getSobreNome());
			elemento.setData(formatada.format(listaDAO.get(i).getData_parecer().getTime()));
			elemento.setJulgamento(listaDAO.get(i).getJulgamento().get());
			elemento.setMotivo(listaDAO.get(i).getMotivoIndeferimento());
			tabela.add(elemento);
		}
		return tabela;
	}
	
	public void salvar(String motivo,TipoParecer tipoParecer){
		Afastamento afastamento = new Afastamento();
		System.out.println(idAfastamento);
		afastamento = aplAfastamento.buscaId(idAfastamento);
		if(afastamento==null){
			System.out.println("Afastamento nulo");
		}
		Parecer parecer = new Parecer();
		
		/*if(!afastamento.getSituacaoSolicitacao().getStatusAfastamento().equals("LIBERADO")){
			//result.redirectTo(IndexController.class).msgErro("Afastamendo não consta como Liberado!");
			return "index/msgErro";
		}*/
		
		Calendar cal = Calendar.getInstance();
		
		parecer.setMotivoIndeferimento(motivo);
		parecer.setJulgamento(tipoParecer);
		parecer.setRelator(usuarioWeb.getLogado());
		parecer.setAfastamento(afastamento);
		parecer.setData_parecer(cal);
		
		aplParecer.salvar(parecer,afastamento,usuarioWeb.getLogado(),tipoParecer);
		//ui.getNavigator().navigateTo("main");
	}

	public String getIdAfastamento() {
		return idAfastamento;
	}

	public void setIdAfastamento(String idAfastamento) {
		this.idAfastamento = idAfastamento;
	}

}

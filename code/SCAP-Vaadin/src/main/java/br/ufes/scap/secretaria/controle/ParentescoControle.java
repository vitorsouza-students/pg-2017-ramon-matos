package br.ufes.scap.secretaria.controle;

import org.springframework.beans.factory.annotation.Autowired;

import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.spring.annotation.UIScope;

import br.ufes.scap.nucleo.dominio.TipoParentesco;
import br.ufes.scap.secretaria.aplicacao.AplParentesco;
import br.ufes.scap.secretaria.aplicacao.AplPessoa;

@SpringComponent
@UIScope
public class ParentescoControle {
	String matricula1;

	@Autowired
	AplPessoa aplPessoa;
	
	@Autowired
	AplParentesco aplParentesco;
	
	public boolean salva(String matricula2,TipoParentesco tipo){
		if(aplPessoa.buscaMatricula(matricula2) == null){
			return false;
		}
		aplParentesco.salvar(matricula1,matricula2,tipo);
		return true;
	}

	public String getMatricula1() {
		return matricula1;
	}

	public void setMatricula1(String matricula1) {
		this.matricula1 = matricula1;
	}
	
}

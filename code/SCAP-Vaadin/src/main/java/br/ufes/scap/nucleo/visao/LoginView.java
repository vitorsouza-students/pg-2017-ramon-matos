package br.ufes.scap.nucleo.visao;

import org.springframework.beans.factory.annotation.Autowired;

import com.vaadin.event.ShortcutAction.KeyCode;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.PasswordField;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;

import br.ufes.scap.nucleo.controle.UsuarioController;

@SpringView(name="login")
public class LoginView extends CustomComponent implements View, ClickListener {

	@Autowired
	UsuarioController userControle;
	
	Label titulo;
	private TextField usernameField;
	private PasswordField passwordField;
	private Button loginButton;

	@Override
	public void enter(ViewChangeEvent event) {

		titulo = new Label("Entre com o seu login");
		usernameField = new TextField("Matricula");
		passwordField = new PasswordField("Password");
		loginButton = new Button("Login");
		loginButton.addClickListener(this);
		loginButton.setClickShortcut(KeyCode.ENTER);

		VerticalLayout layout = new VerticalLayout();
		setCompositionRoot(layout);
		layout.setSizeFull();

		layout.addComponent(titulo);
		layout.addComponent(usernameField);
		layout.addComponent(passwordField);
		layout.addComponent(loginButton);

	}

	@Override
	public void buttonClick(ClickEvent event) {
		String username = usernameField.getValue();
		String password = passwordField.getValue();
		
		if (!userControle.logar(username, password)) {
			new Notification("Matricula ou senha incoreta", Notification.TYPE_ERROR_MESSAGE).show(getUI().getPage());
			return;
		}
		
		getUI().getNavigator().navigateTo("main");
	}
}
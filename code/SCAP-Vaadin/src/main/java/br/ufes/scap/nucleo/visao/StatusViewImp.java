package br.ufes.scap.nucleo.visao;

import org.springframework.beans.factory.annotation.Autowired;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.Button;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.themes.ValoTheme;

import br.ufes.scap.nucleo.controle.AfastamentoControle;
import br.ufes.scap.nucleo.controle.UsuarioController;
import br.ufes.scap.nucleo.dominio.SituacaoSolic;

@SpringView(name = "statusform")
public class StatusViewImp extends CustomComponent implements ClickListener, View {
	Label titulo = new Label("Selecinone o novo status");
	ComboBox<SituacaoSolic> novoStatus = new ComboBox<SituacaoSolic>("Novo Status");
	Button salvar = new Button("Salvar", this);

	@Autowired
	UsuarioController userlogado;

	@Autowired
	AfastamentoControle afastControle;

	@Override
	public void enter(ViewChangeEvent event) {
		VerticalLayout layout = new VerticalLayout();
		setCompositionRoot(layout);

		if (userlogado.tipoPofessor()) {
			novoStatus.setItems(SituacaoSolic.CANCELADO);
		} else {
			novoStatus.setItems(SituacaoSolic.values());
			novoStatus.setValue(SituacaoSolic.INICIADO);
		}
		
		novoStatus.setTextInputAllowed(false);
		novoStatus.setEmptySelectionAllowed(false);
		salvar.addStyleName(ValoTheme.BUTTON_PRIMARY);

		layout.addComponents(new MenuView(), titulo, novoStatus, salvar);
	}

	@Override
	public void buttonClick(ClickEvent event) {
		if(afastControle.mudarStatus(novoStatus.getValue())){
			getUI().getNavigator().navigateTo("main");
		}else{
			new Notification("Somente o solicitante pode CANCELAR \n o pedido de Afstamento", Notification.TYPE_ERROR_MESSAGE).show(getUI().getPage());
		}
		
	}
}
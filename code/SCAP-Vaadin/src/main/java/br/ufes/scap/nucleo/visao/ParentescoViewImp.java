package br.ufes.scap.nucleo.visao;

import org.springframework.beans.factory.annotation.Autowired;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;

import br.ufes.scap.nucleo.dominio.TipoParentesco;
import br.ufes.scap.secretaria.controle.ParentescoControle;
import br.ufes.scap.secretaria.controle.PessoaControle;

@SpringView(name = "parentescoform")
public class ParentescoViewImp extends CustomComponent implements ClickListener, View {
	Label titulo = new Label("Insira os dados da Pessoa");
	TextField matricula = new TextField("Matrícula:");
	ComboBox<TipoParentesco> tipoParentesco = new ComboBox<TipoParentesco>();
	Button salvar = new Button("Salvar", this);

	@Autowired
	ParentescoControle parentescoControle;
	
	@Autowired
	PessoaControle pessoaControle;
	

	@Override
	public void enter(ViewChangeEvent event) {
		VerticalLayout layout = new VerticalLayout();
		setCompositionRoot(layout);

		tipoParentesco.setItems(TipoParentesco.values());
		tipoParentesco.setValue(TipoParentesco.MATRIMONIAL);
		tipoParentesco.setTextInputAllowed(false);
		tipoParentesco.setEmptySelectionAllowed(false);

		layout.addComponents(new MenuView(),titulo, matricula, tipoParentesco, salvar);
	}

	@Override
	public void buttonClick(ClickEvent event) {
		if(!(parentescoControle.salva(matricula.getValue(), tipoParentesco.getValue()))){
			new Notification("Não há cadastro com essa matricula", Notification.TYPE_ERROR_MESSAGE).show(getUI().getPage());
		}else{
			getUI().getNavigator().navigateTo("main");
		}
	}

}

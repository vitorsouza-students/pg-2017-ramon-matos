package br.ufes.scap.nucleo.controle;

import org.springframework.beans.factory.annotation.Autowired;

import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.spring.annotation.UIScope;

import br.ufes.scap.nucleo.aplicacao.AplAfastamento;
import br.ufes.scap.nucleo.aplicacao.AplRelator;
import br.ufes.scap.nucleo.dominio.Afastamento;
import br.ufes.scap.nucleo.dominio.Relator;
import br.ufes.scap.secretaria.aplicacao.AplParentesco;
import br.ufes.scap.secretaria.aplicacao.AplPessoa;

@SpringComponent
@UIScope
public class RelatorControle {
	String idAfastamento;

	@Autowired
	private AplRelator aplRelator;

	@Autowired
	private AplAfastamento aplAfastamento;

	@Autowired
	private AplPessoa aplPessoa;

	@Autowired
	private AplParentesco aplParentesco;

	public boolean salvaPorAfastamento(Relator relator, String matricula, String id_afastamento) {
		Afastamento afastamento = aplAfastamento.buscaId(id_afastamento);
		if (aplPessoa.buscaMatricula(matricula) == null) {
			return false;
		}
		relator.setRelator(aplPessoa.buscaMatricula(matricula));
		if (aplParentesco.checaParentesco(afastamento.getSolicitante().getId_pessoa().toString(),
				relator.getRelator().getId_pessoa().toString())) {
			/*
			 * map.addAttribute(
			 * "Erro: Não foi possível cadastrar essa pessoa como relatora porque existe um parentesco entre ela e o solicitante do afastamento!"
			 * ,matricula);
			 */
			return false;
		} else {
			aplRelator.salvar(relator, afastamento);
			return true;
		}

	}

	public boolean salva(String matricula) {
		Relator relator = new Relator();
		Afastamento afastamento = aplAfastamento.buscaId(idAfastamento);
		if (aplPessoa.buscaMatricula(matricula) == null) {
			return false;
		}
		relator.setRelator(aplPessoa.buscaMatricula(matricula));
		System.out.println(afastamento.getSolicitante().getId_pessoa().toString());
		System.out.println(relator.getRelator().getId_pessoa().toString());
		System.out.println((aplParentesco.checaParentesco(afastamento.getSolicitante().getId_pessoa().toString(),
				relator.getRelator().getId_pessoa().toString())));
		if (aplParentesco.checaParentesco(afastamento.getSolicitante().getId_pessoa().toString(),
				relator.getRelator().getId_pessoa().toString())) {
			/*
			 * map.addAttribute("msg","Erro: Não foi possível cadastrar essa pessoa como relatora porque existe um parentesco entre ela e o solicitante do afastamento!"
			 * );
			 */
			return false;
		} else {
			aplRelator.salvar(relator, afastamento);
			return true;
		}
	}

	public String getIdAfastamento() {
		return idAfastamento;
	}

	public void setIdAfastamento(String idAfastamento) {
		this.idAfastamento = idAfastamento;
	}

}

package br.ufes.scap.nucleo.controle;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import br.ufes.scap.nucleo.aplicacao.AplAfastamento;
import br.ufes.scap.nucleo.aplicacao.AplRelator;
//import br.ufes.scap.nucleo.aplicacao.ChefeRestricted;
import br.ufes.scap.nucleo.dominio.Afastamento;
import br.ufes.scap.nucleo.dominio.Relator;
import br.ufes.scap.secretaria.aplicacao.AplParentesco;
import br.ufes.scap.secretaria.aplicacao.AplPessoa;


@Controller
public class RelatorController {
	
	@Autowired
	private AplRelator aplRelator;
	
	@Autowired
	private AplAfastamento aplAfastamento;
	
	@Autowired
	private AplPessoa aplPessoa;
	
	@Autowired
	private AplParentesco aplParentesco;
	
	//@ChefeRestricted
	@RequestMapping(value = "/relator/form")
	public String formulario(String msg){
		return "relator/formulario";
	}
	
	//@ChefeRestricted
	@RequestMapping(value = "/relator/porafast")
	public void porAfastamento(String msg,String matricula){
		//result.include("msg",msg);
		//result.include("matricula",matricula);
	}
	
	//@ChefeRestricted
	@RequestMapping(value = "/relator/salvaporafast")
	public String salvaPorAfastamento(Relator relator, String matricula,String id_afastamento,HttpSession session,ModelMap map){
		Afastamento afastamento = aplAfastamento.buscaId(id_afastamento);
		relator.setRelator(aplPessoa.buscaMatricula(matricula));
		if(aplParentesco.checaParentesco(afastamento.getSolicitante().getId_pessoa().toString(),relator.getRelator().getId_pessoa().toString())){
			map.addAttribute("Erro: N�o foi poss�vel cadastrar essa pessoa como relatora porque existe um parentesco entre ela e o solicitante do afastamento!",matricula);
			return "relator/porAfastamento";
		}
		
		aplRelator.salvar(relator,afastamento);
		return "index:index";
	}
	
	//@ChefeRestricted
	@RequestMapping(value = "/relator/salva")
	public String salva(Relator relator,String matricula,HttpSession session,ModelMap map){
		Afastamento afastamento = new Afastamento();
		afastamento = (Afastamento)session.getAttribute("afastamento");
		relator.setRelator(aplPessoa.buscaMatricula(matricula));
		if(aplPessoa.buscaMatricula(matricula) == null){
			map.addAttribute("msg","N�o h� nenhum cadastro com essa matricula");
			return "index/msgErro";
		}
		if(relator.getRelator().getTipoPessoa().equals("2")){
			map.addAttribute("msg","Secret�rios n�o podem ser cadastrados como relator");
			return "index/msgErro";
		}
		if(aplParentesco.checaParentesco(afastamento.getSolicitante().getId_pessoa().toString(),relator.getRelator().getId_pessoa().toString())){
			map.addAttribute("msg","N�o foi poss�vel cadastrar essa pessoa como relatora porque existe um parentesco entre ela e o solicitante do afastamento!");
			return "index/msgErro";
		}
		aplRelator.salvar(relator,afastamento);
		return "index/index";
	}

	
}

package br.ufes.scap.secretaria.aplicacao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.ufes.scap.nucleo.dominio.Documento;
import br.ufes.scap.secretaria.persistencia.DocumentoDAO;

@Service
public class AplDocumentoImp implements AplDocumento{

	@Autowired
	private DocumentoDAO documentoDAO;
	
	@Transactional
	@Override
	public void salvar(Documento documento) {
		documentoDAO.salvar(documento);
		
	}
	
	@Transactional
	@Override
	public Documento buscaId(String id_documento) {
		Documento documento = documentoDAO.buscaId(id_documento);
		return documento;
	}
	
	@Transactional
	@Override
	public List<Documento> buscaPorAfastamento(String id_afastamento) {
		return documentoDAO.buscaPorAfastamento(id_afastamento);
	}

}
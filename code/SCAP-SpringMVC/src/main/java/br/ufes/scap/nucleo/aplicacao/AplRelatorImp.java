package br.ufes.scap.nucleo.aplicacao;

import org.springframework.beans.factory.annotation.Autowired;
//import org.apache.commons.mail.Email;
//import org.apache.commons.mail.EmailException;
//import org.apache.commons.mail.SimpleEmail;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.ufes.scap.nucleo.dominio.Afastamento;
import br.ufes.scap.nucleo.dominio.Relator;
import br.ufes.scap.nucleo.dominio.SituacaoSolic;
import br.ufes.scap.nucleo.persistencia.AfastamentoDAO;
import br.ufes.scap.nucleo.persistencia.RelatorDAO;

@Service
public class AplRelatorImp implements AplRelator{

	
	@Autowired
	private AfastamentoDAO afastamentoDAO;
	
	@Autowired
	private RelatorDAO relatorDAO;
	
	@Transactional
	@Override
	public void salvar(Relator relator, Afastamento afastamento) {
		
		relator.setAfastamento(afastamento);
		
		SituacaoSolic situacao = SituacaoSolic.LIBERADO;
		afastamento.setSituacaoSolicitacao(situacao);
		
		afastamentoDAO.merge(afastamento);
		
		/*Email email = new SimpleEmail();
        email.setSubject("SCAP - Voc� foi escolhido como Relator de um Afastamento");
        try {
			email.addTo(relator.getRelator().getEmail());
			email.setMsg("Voc� foi escolhido como Relator do afastamento de ID: " + afastamento.getId_afastamento().toString());
			mailer.asyncSend(email);
			
		} catch (EmailException e) {
			e.printStackTrace();
		}*/
		relatorDAO.salvar(relator);
	}

}

package br.ufes.scap.secretaria.controle;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

//import br.ufes.scap.nucleo.aplicacao.SecretarioRestricted;
//import br.ufes.scap.nucleo.controle.IndexController;
import br.ufes.scap.nucleo.dominio.TipoParentesco;
import br.ufes.scap.secretaria.aplicacao.AplParentesco;
import br.ufes.scap.secretaria.aplicacao.AplPessoa;

@Controller
public class ParentescoController {
	
	@Autowired
	AplParentesco aplParentesco;
	
	@Autowired
	AplPessoa aplPessoa;
	
	//@SecretarioRestricted
	@RequestMapping(value="/parentesco/form", method=RequestMethod.POST)
	public String formulario(String matricula1,ModelMap model){
		model.addAttribute("matricula1",matricula1);
		return "parentesco/formulario";
	}
	
	public void lista(){}
	
	//@SecretarioRestricted
	@RequestMapping(value="/parentesco/salva", method=RequestMethod.POST)
	public String salva(String matricula1,String matricula2,TipoParentesco tipo,ModelMap model){
		if(aplPessoa.buscaMatricula(matricula2) == null){
			model.addAttribute("msg","N�o h� nenhum cadastro com essa matricula para que seja criada rela��o de parentesco");
			return "index/msgErro";
		}
		aplParentesco.salvar(matricula1,matricula2,tipo);
		return "index/index";
	}
	
}

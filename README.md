# README

Projeto de Graduação de Ramon Pereira de Matos: _Aplicação do método FrameWeb no desenvolvimento de um sistema de informação usando os frameworks Spring MVC e Vaadin_.

### Resumo ###

Com ascensão da Internet como um dos principais meios de comunicação, por meio de serviços como a World Wide Web (Web), criou-se uma nova dimensão na utilização de computadores, criando uma demanda por softwares de alta qualidade. Nesse contexto, o desenvolvimento dessas aplicações de maneira _ad-hoc_ se tornou insuficiente e surge a necessidade de aplicar os conceitos da Engenharia de Software na criação destas aplicações Web (WebApp), mas de maneira adaptada a esta nova plataforma de implementação. Nasce, assim, a Engenharia Web.

Para atender requisitos de qualidade e agilidade na criação das aplicações Web, foi proposto o método FrameWeb (_Framework-based Design Method for Web Engeneering_), que sugere a utilização de uma série de _frameworks_ que agilizam o desenvolvimento das WebApps. O método propõe, então, um conjunto de atividades e uma extensão da linguagem UML com quatro tipos de modelos para a fase de projeto. Esses modelos representam conceitos utilizados por algumas categorias de _frameworks_, facilitando, dessa forma, a relação de comunicação entre as equipes de projetistas e a de programadores durante o desenvolvimento da aplicação.

Este trabalho propõe a implementação de duas versões do Sistema de Controle de Afastamentos de Professores (SCAP), uma WebApp que já foi anteriormente implementada utilizando-se o FrameWeb, porém com dois _frameworks_ controladores frontais diferentes do que os utilizados anteriormente. Utilizou-se o Spring MVC e Vaadin com o objetivo de testar a aplicabilidade do método a esses _frameworks_.